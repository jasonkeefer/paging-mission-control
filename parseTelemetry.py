#########################################################
###  script to parse telemetry data, script should    ###
###  should be run using python3, a sample            ###
###  telemetry.txt has been included			      ###
#########################################################

import json
import datetime

satellites = {}
alerts = []

#prompt for path to telemetry data file
path = input('Enter path to file: ')

#open telemetry data file
telemetry_file = open(path,'r')
telemetry_data = telemetry_file.readlines()

for line in telemetry_data:
	#remove trailing newline
	line = line.rstrip()
	
	#map our variables to the data
	timestamp,satelliteId,redHighLimit,yellowHighLimit,yellowLowLimit,redLowLimit,rawValue,component = line.split('|')

	#establish our individual satellites
	if satelliteId not in satellites:
		satellites[satelliteId] = {}
	
	#establish our individual satellite components
	if component not in satellites[satelliteId]:
		satellites[satelliteId][component] = []
	
	#check if we have any violations and store them
	if float(rawValue) > float(redHighLimit):
		alertData = {
			"satelliteId": satelliteId,
			"severity": "RED HIGH",
			"component": component,
			"timestamp": timestamp
		}
		satellites[satelliteId][component].append(alertData)

	if float(rawValue) < float(redLowLimit):
		alertData = {
			"satelliteId": satelliteId,
			"severity": "RED LOW",
			"component": component,
			"timestamp": timestamp
		}
		satellites[satelliteId][component].append(alertData)

#loop through each satellite and each component that failed to meet the threshold
for s in satellites:
	for c in satellites[s]:
		if len(satellites[s][c]) >= 3:
			
			#get the first timestamp for the component in the list
			firstTimestamp = satellites[s][c][0]['timestamp']
			firstTimestamp = datetime.datetime.strptime(firstTimestamp, '%Y%m%d %H:%M:%S.%f')
			
			#get the last timestamp for the component in the list
			lastTimestamp = satellites[s][c][len(satellites[s][c]) -1]['timestamp']
			lastTimestamp = datetime.datetime.strptime(lastTimestamp, '%Y%m%d %H:%M:%S.%f')
			
			#compare timestamps and check if it falls within our requirements
			tdelta = lastTimestamp - firstTimestamp
			delta = datetime.timedelta(minutes=5)
			if tdelta < delta:
				firstTimestamp = firstTimestamp.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'
				satellites[s][c][0]['timestamp'] = firstTimestamp
				alerts.append(satellites[s][c][0])

print (json.dumps(alerts, indent=2))
